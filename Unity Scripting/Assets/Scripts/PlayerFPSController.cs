using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(MouseLook))]
public class PlayerFPSController : MonoBehaviour
{
    private CharacterMovement characterMovement;
    private MouseLook mouseLook;
    
    // Start is called before the first frame update
    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        GameObject.Find("Capsule").gameObject.SetActive(false);
    }

    // Update is called once per frame
    private void Update()
    {
        movement();
        rotation();
    }
    private void movement()
    {
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dahInput = Input.GetButton("Dash");

        characterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dahInput);
    }
    private void rotation()
    {
        float vRotationInput = Input.GetAxis("Mouse Y");
        float hRotationInput = Input.GetAxis("Mouse X");

        mouseLook.handleRotation(hRotationInput, vRotationInput);
    }
}
